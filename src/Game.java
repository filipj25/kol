import java.util.*;

public class Game {
    ArrayList<String> words;
    String chosenWord;
    StringBuilder printedWord ;
    HashSet<Character> guessedLetters;
    HashSet<Character> charsInWord;
    int Attempts;

    public Boolean getFinished() {
        return finished;
    }

    Boolean finished;


    Game(){
        words = new ArrayList<String>() {
            {
                add("Slovo1");
                add("Pes");
                add("Hra");
                add("Babagy");
                add("kek");
            }


        };
        Attempts = 0;
        printedWord = new StringBuilder("");
        finished = false;
        guessedLetters = new HashSet<Character>();
        charsInWord = new HashSet<Character>();

    }
    void chooseWord(){
        Random rng = new Random();
        int index = rng.nextInt(words.size());
        chosenWord = words.get(index);
        System.out.println("Vygeneroval sem slovo, muzes hadat!");
        for(char c: chosenWord.toCharArray()){
            charsInWord.add(c);
            printedWord.append("*");
        }

    }
    void guessLetter(){
        Attempts++;
        Scanner reader = new Scanner(System.in);
        reader.useDelimiter("");
        char c = reader.next().charAt(0);
        if (guessedLetters.contains(c)){
            System.out.println("Already guessed that");
            return;
        }
        if(charsInWord.contains(c)){
            guessedLetters.add(c);
            for (int i = 0; i < chosenWord.length(); i++){
                if (chosenWord.charAt(i) == c){
                    printedWord.setCharAt(i,c);
                }
            }
            if (printedWord.toString().equals(chosenWord)){
                System.out.println("U Won, the word was " + chosenWord );
                System.out.println("It took " + Attempts + " attempts");
                finished = true;
            }
            else {
                System.out.println("Letter was there!, " + printedWord);

            }


        }
        else {
            guessedLetters.add(c);
            System.out.println("Not present");
        }
    }







}
